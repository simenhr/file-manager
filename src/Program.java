import main.java.file_methods.FileLogging;
import main.java.file_methods.FileManipulator;
import main.java.file_methods.ReadWriteFiles;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        
        String[] result;
        String fileName;
        String word;
        boolean isRunning = true;
        FileManipulator fManipulator = new FileManipulator();
        ReadWriteFiles rWriteFiles = new ReadWriteFiles();
        FileLogging logger = new FileLogging();

        while (isRunning == true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Hi there!");
            System.out.println("Please choose one of the following options:");
            System.out.println("1. List files in directory");
            System.out.println("2. Manipulate a file");
            System.out.println("3. Exit");
            System.out.print("Answer: ");
            int userInput = Integer.parseInt(scanner.nextLine().trim());
            switch (userInput) {
                case 1:
                    System.out.println("1. List all files");
                    System.out.println("2. List all .txt files");
                    System.out.println("0. Go back");
                    System.out.print("Answer: ");
                    userInput = Integer.parseInt(scanner.nextLine().trim());
                    switch (userInput) {
                        case 1:
                            System.out.println("Here are all the files:");
                            rWriteFiles.listOfFiles();
                            break;

                        case 2:
                            System.out.println("Here are all the .txt files:");
                            rWriteFiles.listOfFiles(".txt");
                            break;

                        case 0:
                            break;

                        default:
                            System.out.println("Please enter a valid number!");
                    }

                    break;

                case 2:
                    System.out.println("What would you like to do?");
                    System.out.println("1. Get file name");
                    System.out.println("2. Get file size");
                    System.out.println("3. Get lines in .txt file");
                    System.out.println("4. Search for a word in a .txt file");
                    System.out.println("5. Get how many times a word in a .txt file occurs");
                    System.out.println("0. Go back");
                    System.out.print("Answer: ");
                    userInput = Integer.parseInt(scanner.nextLine().trim());
                    switch (userInput) {
                        case 1:
                            System.out.print("Enter file name: ");
                            fileName = scanner.nextLine().trim();
                            result = fManipulator.getFileName(fileName);
                            logger.logGetFileName(result[0], result[1]);
                            break;

                        case 2:
                            System.out.print("Enter file name: ");
                            fileName = scanner.nextLine().trim();
                            result = fManipulator.getFileSize(fileName);
                            logger.logGetFileSize(result[0], result[1], result[2]);
                            break;

                        case 3:
                            System.out.print("Enter file name: ");
                            fileName = scanner.nextLine().trim();
                            result = fManipulator.getFileLines(fileName);
                            logger.logGetFileLines(result[0], result[1], result[2]);
                            break;

                        case 4:
                            System.out.print("Enter file name: ");
                            fileName = scanner.nextLine().trim();
                            System.out.print("Enter word you want to search for: ");
                            word = scanner.nextLine().trim();
                            result = fManipulator.isWordInTxt(word, fileName);
                            logger.logIsWordInText(result[0], result[1], result[2], result[3]);
                            break;

                        case 5:
                            System.out.print("Enter file name: ");
                            fileName = scanner.nextLine().trim();
                            System.out.print("Enter word you want to search for: ");
                            word = scanner.nextLine().trim();
                            result = fManipulator.getCountWord(word, fileName);
                            logger.logGetWordCount(result[0], result[1], result[2], result[3]);
                            ;
                            break;

                        case 0:
                            break;

                        default:
                            System.out.println("Please enter a valid number! ");

                    }
                    break;

                case 3:
                    isRunning = false;
                    break;

                default:
                    System.out.println("Please enter a valid number!");
            }
        }
    }
}
