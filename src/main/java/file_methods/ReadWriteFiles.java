package main.java.file_methods;

import java.io.File;

public class ReadWriteFiles {

    public static void listOfFiles(String extension) {
        File folder = new File("../src/main/resources");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile() && listOfFiles[i].getName().endsWith(extension)) {
                System.out.println("File " + listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
        }
    }

    public static void listOfFiles() {
        File folder = new File("../src/main/resources");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("File name: " + listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory name: " + listOfFiles[i].getName());
            }
        }
    }

    public static void main(String[] args) {
        listOfFiles();
    }
}
