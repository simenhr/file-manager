package main.java.file_methods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class FileManipulator {

    public static final String PATH = "../src/main/resources/";

    public String[] getFileName(String fileName) {
        long timeStart = System.currentTimeMillis();
        File file;
        String[] result = new String[2];
        try {
            if (new File(PATH + fileName + ".txt").exists() == true) {
                file = new File(PATH + fileName + ".txt");
                result[0] = file.getName();
                System.out.println("Name of the file is " + result[0]);
            }
            else {
                System.out.println("File does not exist");
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("File does not exist, or must be a .txt file");
        }

        String time = String.valueOf(System.currentTimeMillis() - timeStart);


        result[1] = time;
        return result;
    }

    public String[] getFileSize(String fileName) {
        long timeStart = System.currentTimeMillis();
        File file;
        try {
            file = new File(PATH + fileName + ".txt");
            if(file.length() == 0) {
                System.out.println("No such file");
            }
            System.out.println("Size of the file " + fileName + " is " +
                    String.valueOf(file.length()) + "bytes");

        } catch (Exception e) {
            throw new IllegalArgumentException("File does not exist, or must be a .txt file");
        }
        String time = String.valueOf(System.currentTimeMillis() - timeStart);
        String[] result = new String[3];
        result[0] = String.valueOf(file.length());
        result[1] = fileName;
        result[2] = time;
        return result;
    }

    public String[] getFileLines(String fileName) {
        long timeStart = System.currentTimeMillis();
        int lines = 0;
        try {
            if (new File(PATH + fileName + ".txt").exists() == true) {
                BufferedReader reader = new BufferedReader(new FileReader(PATH + fileName + ".txt"));
                while (reader.readLine() != null) {
                    lines++;
                }
                System.out.println(String.valueOf(lines));
                reader.close();
            } else {
                System.out.println("No such file");
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("File does not exist, or must be a .txt file");
        }
        String time = String.valueOf(System.currentTimeMillis() - timeStart);
        String[] result = new String[3];
        result[0] = String.valueOf(lines);
        result[1] = fileName;
        result[2] = time;
        return result;
    }

    public String[] isWordInTxt(String word, String fileName) {
        long timeStart = System.currentTimeMillis();
        boolean wordInText = false;
        try {
            if (new File(PATH + fileName + ".txt").exists() == true) {
                BufferedReader reader = new BufferedReader(new FileReader(PATH + fileName + ".txt"));
                while (reader.readLine() != null) {
                    String nextLine = reader.readLine();
                    String[] array = nextLine.split("\\W+");
                    for (String wordToCompare : array) {
                        if (wordToCompare.equalsIgnoreCase(word)) {
                            wordInText = true;
                        }
                    }
                }
                reader.close();
            } else {
                System.out.println("No such file");
            }
        } catch (Exception e) {
        }
        String time = String.valueOf(System.currentTimeMillis() - timeStart);
        String[] result = new String[4];
        result[0] = word;
        result[1] = String.valueOf(wordInText);
        result[2] = fileName;
        result[3] = time;
        if (wordInText == true) {
            System.out.println(word + " is in the .txt file");
        } else {
            System.out.println(word + " is not in the .txt file");
        }
        return result;
    }

    public static String[] getCountWord(String word, String fileName) {
        long timeStart = System.currentTimeMillis();
        int wordCount = 0;

        try {
            if (new File(PATH + fileName + ".txt").exists() == true) {
                BufferedReader reader = new BufferedReader(new FileReader(PATH + fileName + ".txt"));
                while (reader.readLine() != null) {
                    String nextLine = reader.readLine();
                    String[] array = nextLine.split("\\W+");
                    for (String wordToCompare : array) {
                        if (wordToCompare.equalsIgnoreCase(word)) {
                            wordCount++;
                        }
                    }
                }
                System.out.println(word + " appears " + wordCount + " times in " + fileName);
                reader.close();
            } else {
                System.out.println("No such file");
            }
        } catch (Exception e) {
        }
        String time = String.valueOf(System.currentTimeMillis() - timeStart);
        String[] result = new String[4];
        result[0] = word;
        result[1] = String.valueOf(wordCount);
        result[2] = fileName;
        result[3] = time;
        System.out.println(word + " appears " + wordCount + " times in " + fileName);
        return result;

    }
}
