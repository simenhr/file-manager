package main.java.file_methods;

import java.io.*;
import java.time.LocalDateTime;

public class FileLogging {

    public static final String PATH = "../src/main/resources";
    public static final String TXT_FILE_NAME = "/logger.txt";
    private LocalDateTime date = LocalDateTime.now();

    public FileLogging() {
        //
    }

    public void logGetFileName(String fileName, String time) {
        try {
            File txtFile = new File(PATH + TXT_FILE_NAME);
            if (txtFile.createNewFile()) {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": The file name " + fileName + " was searched for, and the function took " + time
                        + "ms to execute.");
                writer.append("\n");
                writer.close();
            } else {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": The file name " + fileName + " was searched for, and the function took " + time
                        + "ms to execute.");
                writer.append("\n");
                writer.close();
            }

        } catch (Exception e) {
        }
    }

    public void logGetFileSize(String fileSize, String fileName, String time) {
        try {
            File txtFile = new File(PATH + TXT_FILE_NAME);
            if (txtFile.createNewFile()) {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": A search of the file size of " + fileName + " was executed, return the size" + fileSize
                        + "byte. The function took " + time + "ms to execute.");
                writer.append("\n");
                writer.close();
            } else {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": A search of the file size of " + fileName + " was executed, return the size" + fileSize
                        + "byte. The function took " + time + "ms to execute.");
                writer.append("\n");
                writer.close();
            }

        } catch (Exception e) {
        }
    }

    public void logGetFileLines(String fileLines, String fileName, String time) {
        try {
            File txtFile = new File(PATH + TXT_FILE_NAME);
            if (txtFile.createNewFile()) {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": A search for the number of lines in file " + fileName + " was " + fileLines
                        + " was executed, and the function took " + time + "ms to execute.");
                writer.append("\n");
                writer.close();
            } else {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": A search for the number of lines in file " + fileName + " was " + fileLines
                        + " was executed, and the function took " + time + "ms to execute.");
                writer.append("\n");
                writer.close();
            }

        } catch (Exception e) {
        }
    }

    public void logIsWordInText(String word, String isWordInText, String fileName, String time) {
        try {
            File txtFile = new File(PATH + TXT_FILE_NAME);
            if (txtFile.createNewFile()) {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": The word " + word + " was looked after in file " + fileName
                        + " and returned " + isWordInText + ". The function took " + time + "ms to execute.");
                writer.append("\n");
                writer.close();
            } else {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": The word " + word + " was looked after in file " + fileName
                        + " and returned " + isWordInText + ". The function took " + time + "ms to execute.");
                writer.append("\n");
                writer.close();
            }

        } catch (Exception e) {
        }
    }

    public void logGetWordCount(String word, String wordCount, String fileName, String time){
        try {
            File txtFile = new File(PATH + TXT_FILE_NAME);
            if (txtFile.createNewFile()) {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": The word " + word + " was looked after in file " + fileName
                        + " and was found " + wordCount + " times. The function took " + time + "ms to execute.");
                writer.append("\n");
                writer.close();
            } else {
                BufferedWriter writer = new BufferedWriter(new FileWriter(PATH + TXT_FILE_NAME, true));
                writer.append(date + ": The word " + word + " was looked after in file " + fileName
                        + " and was found " + wordCount + " times. The function took " + time + "ms to execute.");
                writer.append("\n");
                writer.close();
            }

        } catch (Exception e) {
        }
    }

}
