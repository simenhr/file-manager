# File Manager

A terminal program to read and manipulate files in a folder.

## Getting Started

### Compile the classes

To compile all the classes to the `./out/`, the following command can be written in the terminal in the project folder:

![compile](/uploads/ee4a7cac4de7ff2e710cfa0dc622e444/compile.PNG)

### Create .jar

To gather all the classes in a .jar, you can type the following after changing your directory to `./out/`:

![create-jar](/uploads/23a4161b2e7b4a9f0a5e692beda4fb99/create-jar.PNG)

### Run .jar

The .jar can be run with the following command in `./out/`:

![run-jar](/uploads/26cd9093a7920c5fca3f5c8f99fa6e93/run-jar.PNG)

## Functionalities

The program lets you list files in a given directory (set to `src/main/java/resources` in the program) and to manipulate them by;
1.  Get a file's name
2.  Get a file's size
3.  Get the number of lines in a text file
4.  Search if a word is found in a text
5.  Search how many times a word occurs in a text

### Logger

The program logs every file manipulation the user does in a `logger.txt` file, and is stored in `src/main/java/resources`.


## Authors

* **Simen Røstum** - *Initial work*

